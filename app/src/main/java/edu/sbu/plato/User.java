package edu.sbu.plato;

import android.widget.ImageView;

public class User {
    private String username,password,bio;
    private int score;
    private ImageView profilePic;

    public User(String username, String password) {
        this.username = username;
        this.password = password;
    }

    public User(String username, String password, ImageView profilePic){
        this.username = username;
        this.password = password;
        this.profilePic=profilePic;
        score = 0;
        bio = null;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getBio() {
        return bio;
    }

    public void setBio(String bio) {
        this.bio = bio;
    }

    public int getScore() {
        return score;
    }

    public void setScore(int score) {
        this.score = score;
    }

    public ImageView getProfilePic() {
        return profilePic;
    }

    public void setProfilePic(ImageView profilePic) {
        this.profilePic = profilePic;
    }
}
