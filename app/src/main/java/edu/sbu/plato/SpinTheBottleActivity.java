package edu.sbu.plato;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.RotateAnimation;
import android.widget.ImageView;

import java.util.Random;

public class SpinTheBottleActivity extends AppCompatActivity {
    private Random random = new Random();
    private int lastDir;
    private boolean spinning;
    private ImageView img_bottle;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_spin_the_bottle);

        img_bottle = (ImageView) findViewById(R.id.IMG_bottle);
    }

    public void spinTheBottle(View view) {
        if (!spinning) {
            int newDir = random.nextInt(2000);
            float pivotX = img_bottle.getWidth() / 2;
            float pivotY = img_bottle.getHeight() / 2;
            Animation rotate = new RotateAnimation(lastDir, newDir, pivotX, pivotY);
            rotate.setDuration(3500);
            rotate.setFillAfter(true);
            rotate.setAnimationListener(new Animation.AnimationListener() {
                @Override
                public void onAnimationStart(Animation animation) {
                    spinning = true;
                }

                @Override
                public void onAnimationEnd(Animation animation) {
                    spinning = false;
                }

                @Override
                public void onAnimationRepeat(Animation animation) {

                }
            });


            lastDir = newDir;
            img_bottle.startAnimation(rotate);
        }
    }
}