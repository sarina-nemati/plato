package edu.sbu.plato;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import java.io.FileInputStream;
import java.io.ObjectInputStream;
import java.util.ArrayList;

import edu.sbu.plato.UI.Home.HomeFragment;

public class LoginActivity extends AppCompatActivity  {

    private EditText et_username , et_password;
    private Button btn_done;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        et_username = (EditText) findViewById(R.id.ET_username);
        String str_username = et_username.getText().toString();

        et_password = (EditText) findViewById(R.id.ET_password);
        String str_password = et_password.getText().toString();

        btn_done = (Button) findViewById(R.id.BTN_done);
        btn_done.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                gotoHomePageActivity();
            }
        });

    }

    private void gotoHomePageActivity() {
        Intent intent = new Intent(this, HomePageActivity.class);
        startActivity(intent);
        finish();
    }

    private boolean checkUsernameExistence(){
        return true;
    }

    private boolean checkCorrectPassword(){
        return true;
    }
}