package edu.sbu.plato;

import androidx.appcompat.app.AppCompatActivity;

import android.media.AudioManager;
import android.media.SoundPool;
import android.os.Bundle;
import android.view.View;

public class DrumPadActivity extends AppCompatActivity {

    private int sound00,sound1,sound2,sound3,sound4,sound5,sound6,sound7,sound8,sound9;
    private SoundPool sp;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_drum_pad);

        sp = new SoundPool(2, AudioManager.STREAM_MUSIC , 0);
        sound00 = sp.load(getApplicationContext() , R.raw.sound00,1);
        sound1 = sp.load(getApplicationContext() , R.raw.sound1,1);
        sound2 = sp.load(getApplicationContext() , R.raw.sound2,1);
        sound3 = sp.load(getApplicationContext() , R.raw.sound3,1);
        sound4 = sp.load(getApplicationContext() , R.raw.sound4,1);
        sound5 = sp.load(getApplicationContext() , R.raw.sound5,1);
        sound6 = sp.load(getApplicationContext() , R.raw.sound6,1);
        sound7 = sp.load(getApplicationContext() , R.raw.sound7,1);
        sound8 = sp.load(getApplicationContext() , R.raw.sound8,1);
        sound9 = sp.load(getApplicationContext() , R.raw.sound9,1);
    }

    public void playSound1(View view){
        sp.play(sound1,1.0f,1.0f,0,0,1.0f);
    }
    public void playSound2(View view){
        sp.play(sound2,1.0f,1.0f,0,0,1.0f);
    }
    public void playSound3(View view){
        sp.play(sound3,1.0f,1.0f,0,0,1.0f);
    }
    public void playSound4(View view){
        sp.play(sound4,1.0f,1.0f,0,0,1.0f);
    }
    public void playSound5(View view){
        sp.play(sound5,1.0f,1.0f,0,0,1.0f);
    }
    public void playSound6(View view){
        sp.play(sound6,1.0f,1.0f,0,0,1.0f);
    }
    public void playSound7(View view){
        sp.play(sound7,1.0f,1.0f,0,0,1.0f);
    }
    public void playSound8(View view){
        sp.play(sound8,1.0f,1.0f,0,0,1.0f);
    }
    public void playSound9(View view){
        sp.play(sound9,1.0f,1.0f,0,0,1.0f);
    }
}