package edu.sbu.plato.UI.Chat;

import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AbsListView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import edu.sbu.plato.MainActivity;
import edu.sbu.plato.Message;
import edu.sbu.plato.R;

public class ChatFragment extends Fragment {

    private List<Message> list;
    private EditText et_msg;
    private Button btn_send;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View root = inflater.inflate(R.layout.fragment_chat,container,false);

        Message[] items = { new Message("Sarina","slmm!","11:00 a.m.") ,
                new Message("A","slm!fer?","11:01 a.m.") ,
                new Message("S","gtr tyhft gtr?!","11:02 a.m.") ,
                new Message("A","mggnm sdevfx","11:03 a.m.") ,
                new Message("A","vfdsfvx","11:04 a.m.") ,
                new Message("S","codfdse mizvfdnm","11:05 a.m.") ,
                new Message("A","tmofdzsm nfssu?!","11:06 a.m.") ,
                new Message("S","nafdz!","11:07 a.m.") ,
                new Message("A","p3dawareyia","11:08 a.m.") ,
                new Message("S","shafvcdzdiid","11:09 a.m.") ,
                new Message("A","talsbfdh kfvghe","11:10 a.m.") ,
                new Message("S","dfdrm msfknm","11:11 a.m.") ,
                new Message("S","vfdli nemidfzsrsm","11:12 a.m.") ,
                new Message("A","mirdzvcesia","11:13 a.m.") ,
                new Message("A","ngrndfzbsh","11:14 a.m.") ,
                new Message("S","okfds brfdsfxfelan","11:15 a.m.") ,
                new Message("S","bfds!","11:20 a.m.")
        };

        list = new ArrayList<>();
        list.addAll(Arrays.asList(items));

        et_msg = (EditText) root.findViewById(R.id.ET_messageBody) ;
        btn_send = (Button) root.findViewById(R.id.BTN_send);
        btn_send.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!et_msg.getText().toString().isEmpty()){
                Message msg = new Message("A",et_msg.getText().toString(),"11:30 a.m.");
                list.add(msg);}
                else{
                    Toast.makeText(getActivity(), "Your message body can not be empty...", Toast.LENGTH_SHORT).show();
                }
            }
        });


        RecyclerView recyclerView = root.findViewById(R.id.RV_messageList);
        recyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
        MessageListAdapter adapter =  new MessageListAdapter(list);
        recyclerView.setAdapter(adapter);
        recyclerView.setItemAnimator(new DefaultItemAnimator());


        return root;
    }
}
