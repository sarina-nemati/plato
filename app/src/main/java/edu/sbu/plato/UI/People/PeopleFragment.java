package edu.sbu.plato.UI.People;

import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.viewpager.widget.ViewPager;

import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.android.material.tabs.TabItem;
import com.google.android.material.tabs.TabLayout;

import edu.sbu.plato.AddNewPeopleActivity;
import edu.sbu.plato.PagerAdapetrPeopleFragment;
import edu.sbu.plato.R;

public class PeopleFragment extends Fragment {

    private View root;
    private FloatingActionButton fab_search;
    private TabLayout tabLayout_peopleFragment;
    private ViewPager viewPager_peopleFragment;
    private TabItem tab_friends , tab_topPlayers;
    private PagerAdapetrPeopleFragment pagerAdapetrPeopleFragment;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);


    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        root = inflater.inflate(R.layout.fragment_people, container, false);


        tabLayoutStuff();
        fabStuff();



        return root;
    }

    public void gotoAddNewPeopleActivity() {
        Intent intent = new Intent(getActivity(), AddNewPeopleActivity.class);
        startActivity(intent);
    }

    private void fabStuff(){
        fab_search = (FloatingActionButton) root.findViewById(R.id.BTN_addNewPeople);
        fab_search.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                gotoAddNewPeopleActivity();
            }
        });
    }

    private void tabLayoutStuff(){
        tabLayout_peopleFragment = (TabLayout) root.findViewById(R.id.tabBarPeopleFragment);
        tab_friends = (TabItem) root.findViewById(R.id.tab_friends);
        tab_topPlayers = (TabItem) root.findViewById(R.id.tab_topPlayers);
        viewPager_peopleFragment = (ViewPager) root.findViewById(R.id.viewPager_peopleFragment);
        pagerAdapetrPeopleFragment = new PagerAdapetrPeopleFragment(getFragmentManager() , tabLayout_peopleFragment.getTabCount());
        viewPager_peopleFragment.setAdapter(pagerAdapetrPeopleFragment);
        tabLayout_peopleFragment.addOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                viewPager_peopleFragment.setCurrentItem(tab.getPosition());
            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {

            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {

            }
        });
    }


}
