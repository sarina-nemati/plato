package edu.sbu.plato.UI.Chat;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import java.util.List;

import edu.sbu.plato.Message;
import edu.sbu.plato.R;

public class MessageListAdapter extends RecyclerView.Adapter {
    private List<Message> list;

    MessageListAdapter(List<Message> list){
        this.list = list;
    }


    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.recyclerview_item_message,parent,false);
        return new MessageViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position) {
        MessageViewHolder messageViewHolder = (MessageViewHolder)holder;
        messageViewHolder.bind(list.get(position));
    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    private static class MessageViewHolder extends RecyclerView.ViewHolder{
        TextView senderID , messageBody , messageDate ;


        MessageViewHolder(@NonNull View itemView) {
            super(itemView);
            senderID = itemView.findViewById(R.id.TV_senderID);
            messageBody = itemView.findViewById(R.id.TV_message);
            messageDate = itemView.findViewById(R.id.TV_messageTime);
        }

        void bind(Message message){
            messageBody.setText(message.getMessageBody());
            messageDate.setText(message.getMessageTime());
            senderID.setText(message.getSenderID());
        }
    }
}
