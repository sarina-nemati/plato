package edu.sbu.plato.UI.Games;

import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import edu.sbu.plato.DotsAndBoxesActivity;
import edu.sbu.plato.DrumPadActivity;
import edu.sbu.plato.FindLetterActivity;
import edu.sbu.plato.Puzzle2DActivity;
import edu.sbu.plato.R;
import edu.sbu.plato.SpinTheBottleActivity;
import edu.sbu.plato.TicTacToeActivity;


public class GamesFragment extends Fragment {

    private ImageView img_xoGame, img_findLetterGame,img_dotsAndBoxesGame,img_spinTheBottleGame,img_drumPadGame,img_2DPuzzleGame;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View root = inflater.inflate(R.layout.fragment_games, container, false);

        img_dotsAndBoxesGame = (ImageView) root.findViewById(R.id.ic_dots_boxes);
        img_dotsAndBoxesGame.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                gotoDotsAndBoxesGame();
            }
        });

        img_xoGame = (ImageView) root.findViewById(R.id.ic_XO);
        img_xoGame.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                gotoXOGame();
            }
        });

        img_findLetterGame = (ImageView) root.findViewById(R.id.ic_find_letter);
        img_findLetterGame.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                gotoFindLetterGame();
            }
        });

        img_spinTheBottleGame = (ImageView) root.findViewById(R.id.ic_spin_the_bottle);
        img_spinTheBottleGame.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) { gotoSpinTheBottleGame();
            }
        });

        img_drumPadGame = (ImageView) root.findViewById(R.id.ic_drum_pad);
        img_drumPadGame.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                gotoDrumPadGame();
            }
        });

        img_2DPuzzleGame = (ImageView) root.findViewById(R.id.ic_2d_puzzle);
        img_2DPuzzleGame.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                goto2DPuzzleGame();
            }
        });

        return root;

    }

    private void gotoXOGame(){
        Intent intent = new Intent(getActivity(), TicTacToeActivity.class);
        startActivity(intent);
    }

    private void gotoDotsAndBoxesGame(){
        Intent intent = new Intent(getActivity(), DotsAndBoxesActivity.class);
        startActivity(intent);
    }

    private void gotoFindLetterGame(){
        Intent intent = new Intent(getActivity(), FindLetterActivity.class);
        startActivity(intent);
    }

    private void gotoSpinTheBottleGame(){
        Intent intent = new Intent(getActivity(), SpinTheBottleActivity.class);
        startActivity(intent);
    }

    private void gotoDrumPadGame(){
        Intent intent = new Intent(getActivity(), DrumPadActivity.class);
        startActivity(intent);
    }

    private void goto2DPuzzleGame(){
        Intent intent = new Intent(getActivity(), Puzzle2DActivity.class);
        startActivity(intent);
    }
}
