package edu.sbu.plato;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentTransaction;

import android.content.Intent;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.widget.Toast;

import com.google.android.material.bottomnavigation.BottomNavigationView;

import edu.sbu.plato.UI.Chat.ChatFragment;
import edu.sbu.plato.UI.Games.GamesFragment;
import edu.sbu.plato.UI.Home.HomeFragment;
import edu.sbu.plato.UI.People.PeopleFragment;

public class GamesActivity extends AppCompatActivity {

    private BottomNavigationView bottomNavigationView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_games);


        bottomNavigationStuff();
    }

    private void bottomNavigationStuff() {
        bottomNavigationView = this.findViewById(R.id.BTN_navigation);
        bottomNavigationView.setOnNavigationItemSelectedListener(new BottomNavigationView.OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(@NonNull MenuItem item) {
                switch (item.getItemId()) {
                    case R.id.BTN_navigation_people:
                        PeopleFragment peopleFragment = new PeopleFragment();
                        openFragment(peopleFragment);
                        break;

                    case R.id.BTN_navigation_chats:
                        ChatFragment chatFragment = new ChatFragment();
                        openFragment(chatFragment);
                        break;

                    case R.id.BTN_navigation_home:
                        HomeFragment homeFragment = new HomeFragment();
                        openFragment(homeFragment);
                        break;

                    case R.id.BTN_navigation_games:
                        GamesFragment gamesFragment = new GamesFragment();
                        openFragment(gamesFragment);
                        break;
                }
                return true;
            }
        });
    }

    public void openFragment(Fragment fragment) {
        FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
        transaction.replace(R.id.fragmentContainer, fragment);
        transaction.addToBackStack(null);
        transaction.commit();
    }
}