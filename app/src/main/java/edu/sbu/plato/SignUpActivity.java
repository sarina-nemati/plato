package edu.sbu.plato;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import edu.sbu.plato.UI.Home.HomeFragment;

public class SignUpActivity extends AppCompatActivity {

    private ArrayList<User> usersList;
    private Button btn_done;
    private EditText et_password, et_password2, et_username;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sign_up);

        usersList = new ArrayList<>();
        usersList.add(new User("user1", "12345"));

        et_password = (EditText) findViewById(R.id.ET_password);
        et_password2 = (EditText) findViewById(R.id.ET_password2);
        et_username = (EditText) findViewById(R.id.ET_username);
        btn_done = (Button) findViewById(R.id.BTN_done);
        btn_done.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                pressDONE();
            }
        });


    }

    private void gotoHomePageActivity() {
        Intent intent = new Intent(this, HomePageActivity.class);
        startActivity(intent);
        finish();
    }

    private boolean checkPasswords() {
        if (!et_password.getText().toString().equals(et_password2.getText().toString())) {
            Toast.makeText(SignUpActivity.this, "Check ypu Passwords Again", Toast.LENGTH_LONG).show();
            return false;
        }
        return true;
    }

    private boolean checkPasswordLength() {
        if (et_password.getText().toString().length() < 5) {
            Toast.makeText(SignUpActivity.this, "Your password must be at least 5 characters", Toast.LENGTH_LONG).show();
            return false;
        }
        return true;
    }

    private ArrayList<User> Deserialize() {
        ArrayList<User> usersList2 = null;
        try {
            FileInputStream fis = openFileInput("E:\\PLATO\\app\\src\\main\\java\\edu\\sbu\\plato\\SAVEUSERS.txt");
            ObjectInputStream in = new ObjectInputStream(fis);
            usersList2 = (ArrayList<User>) in.readObject();
            in.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return usersList2;
    }

    private void Serialize() {
        ArrayList<User> usersList2 = new ArrayList<>(usersList);
        try {
            FileOutputStream fos = openFileOutput("E:\\PLATO\\app\\src\\main\\java\\edu\\sbu\\plato\\SAVEUSERS.txt",MODE_PRIVATE);
            ObjectOutputStream out = new ObjectOutputStream(fos);
            out.writeObject(usersList2);
            out.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private boolean checkUsernameExistence() {
        User user = new User(et_username.getText().toString(), et_password.getText().toString());
        usersList = Deserialize();
        if (usersList.contains(user)) {
            Toast.makeText(SignUpActivity.this, "This username already existed", Toast.LENGTH_LONG).show();
            return false;
        } else {
            usersList.add(user);
            return true;
        }
    }

    private void pressDONE(){
        if (checkPasswords() && checkPasswordLength() && checkUsernameExistence()){
            Serialize();
            gotoHomePageActivity();
        } else {
            Toast.makeText(SignUpActivity.this , "ERROR" , Toast.LENGTH_LONG).show();
        }
    }
}