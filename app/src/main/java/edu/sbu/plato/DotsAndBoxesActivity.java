package edu.sbu.plato;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import java.util.Random;

public class DotsAndBoxesActivity extends AppCompatActivity {
    private Button btn_rock, btn_paper, btn_scissors;
    private ImageView img_compChoice, img_HumanChoice;
    private TextView tv_score;
    private int compScore, humanScore;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        tv_score = (TextView) findViewById(R.id.TV_scores);
        img_compChoice = (ImageView) findViewById(R.id.IMG_compChoice);
        img_HumanChoice = (ImageView) findViewById(R.id.IMG_humanChoice);
        btn_paper = (Button) findViewById(R.id.BTN_paper);
        btn_rock = (Button) findViewById(R.id.BTN_rock);
        btn_scissors = (Button) findViewById(R.id.BTN_scissors);

        btn_paper.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                img_HumanChoice.setImageResource(R.drawable.paper);
                String msg = playTurn("paper");
                Toast.makeText(DotsAndBoxesActivity.this, msg, Toast.LENGTH_SHORT).show();
                tv_score.setText("Score: Human " + Integer.toString(humanScore) + " Computer " + Integer.toString(compScore));
            }
        });
        btn_rock.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                img_HumanChoice.setImageResource(R.drawable.stone);
                String msg = playTurn("rock");
                Toast.makeText(DotsAndBoxesActivity.this, msg, Toast.LENGTH_SHORT).show();
                tv_score.setText("Score: Human " + Integer.toString(humanScore) + " Computer " + Integer.toString(compScore));
            }
        });
        btn_scissors.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                img_HumanChoice.setImageResource(R.drawable.scissor);
                String msg = playTurn("scissors");
                Toast.makeText(DotsAndBoxesActivity.this, msg, Toast.LENGTH_SHORT).show();
                tv_score.setText("Score: Human " + Integer.toString(humanScore) + " Computer " + Integer.toString(compScore));
            }
        });

    }

    private String playTurn(String playerChoice) {
        String computerChoice = "";
        Random random = new Random();
        int computerChoice_num = random.nextInt(3) + 1;

        if (computerChoice_num == 1) {
            computerChoice = "rock";
        } else if (computerChoice_num == 2) {
            computerChoice = "paper";
        } else if (computerChoice_num == 3) {
            computerChoice = "scissors";
        }

        if (computerChoice.equals("scissors")) {
            img_compChoice.setImageResource(R.drawable.scissor);
        } else if (computerChoice.equals("paper")) {
            img_compChoice.setImageResource(R.drawable.paper);
        } else if (computerChoice.equals("rock")) {
            img_compChoice.setImageResource(R.drawable.stone);
        }

        if (computerChoice.equals(playerChoice)) {
            return "Draw.";
        } else if (computerChoice.equals("rock") && playerChoice.equals("paper")) {
            humanScore++;
            return "You win.";
        } else if (computerChoice.equals("paper") && playerChoice.equals("rock")) {
            compScore++;
            return "Computer wins.";
        } else if (computerChoice.equals("rock") && playerChoice.equals("scissors")) {
            compScore++;
            return "Computer wins.";
        } else if (computerChoice.equals("scissors") && playerChoice.equals("rock")) {
            humanScore++;
            return "You win.";
        } else if (computerChoice.equals("scissors") && playerChoice.equals("paper")) {
            compScore++;
            return "Computer wins.";
        } else if (computerChoice.equals("paper") && playerChoice.equals("scissors")) {
            humanScore++;
            return "You win.";
        } else {
            return "";
        }
    }
}