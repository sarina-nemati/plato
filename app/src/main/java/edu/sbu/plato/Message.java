package edu.sbu.plato;

public class Message {
    private String senderID, messageTime, messageBody;

    public Message(String senderID, String messageBody, String messageTime) {
        this.messageBody = messageBody;
        this.messageTime = messageTime;
        this.senderID = senderID;
    }

    public String getMessageBody() {
        return messageBody;
    }

    public String getMessageTime() {
        return messageTime;
    }

    public String getSenderID() {
        return senderID;
    }

    public void setMessageBody(String messageBody) {
        this.messageBody = messageBody;
    }

    public void setMessageTime(String messageTime) {
        this.messageTime = messageTime;
    }

    public void setSenderID(String senderID) {
        this.senderID = senderID;
    }
}
