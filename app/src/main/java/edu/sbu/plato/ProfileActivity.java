package edu.sbu.plato;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import java.io.InputStreamReader;

public class ProfileActivity extends AppCompatActivity {

    private ImageView img_editProfilePic;
    private TextView tv_editUsername;
    private EditText et_bio;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_profile);


        img_editProfilePic = (ImageView) findViewById(R.id.ic_addProfilePic);
        img_editProfilePic.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                editProfilePic();
            }
        });

        tv_editUsername = (TextView) findViewById(R.id.TV_editID);
        tv_editUsername.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                editUsername();
            }
        });

        et_bio = (EditText) findViewById(R.id.ET_bio);
        saveBioInFile();
    }

    private void editProfilePic(){

    }

    private void editUsername(){

    }

    private void saveBioInFile(){}

    public void gotoBackActivity(View view){
        Intent intent = new Intent(this,HomePageActivity.class);
        startActivity(intent);
    }

    public void gotoSettingsActivity(View view){
        Intent intent = new Intent(this,SettingsActivity.class);
        startActivity(intent);
    }

}